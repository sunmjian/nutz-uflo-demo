package net.wendal.uflo.utils;

import org.hibernate.SessionFactory;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.springframework.transaction.PlatformTransactionManager;

import com.bstek.uflo.env.EnvironmentProvider;

@IocBean(factory="getSelf")
public class UfloEnvironmentProvider implements EnvironmentProvider {
    
    // 通过这个单例,让spring和nutz各取所需
    protected static EnvironmentProvider self = new UfloEnvironmentProvider();
    public static EnvironmentProvider getSelf() {
        return self;
    }

    // uflo所需要的东西, 是spring注入的
    protected SessionFactory sessionFactory;
    protected PlatformTransactionManager platformTransactionManager;
    // nutz的注解可以生效
    @Inject
    protected Dao dao;

    public String getCategoryId() {
        return null;
    }

    public String getLoginUser() {
        return "anonymous"; // TODO 通过shiro获取用户名?
    }

    // 后面的是固定方法, 不要动!
    public PlatformTransactionManager getPlatformTransactionManager() {
        return platformTransactionManager;
    }

    public void setPlatformTransactionManager(PlatformTransactionManager platformTransactionManager) {
        this.platformTransactionManager = platformTransactionManager;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
