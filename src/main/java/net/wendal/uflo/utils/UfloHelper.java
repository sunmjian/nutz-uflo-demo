package net.wendal.uflo.utils;

import javax.sql.DataSource;

import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.mvc.Mvcs;

/**
 * uflo需要用到Nutz的数据源和配置信息, 通过spring ioc的factory method共享给他
 * @author Administrator
 *
 */
public class UfloHelper {

    public DataSource getDataSource() {
        return Mvcs.ctx().getDefaultIoc().get(DataSource.class);
    }
    
    public PropertiesProxy getPropertiesProxy() {
        return Mvcs.ctx().getDefaultIoc().get(PropertiesProxy.class, "conf");
    }
}
