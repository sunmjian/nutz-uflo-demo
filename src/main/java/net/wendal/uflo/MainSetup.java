package net.wendal.uflo;

import org.nutz.dao.Dao;
import org.nutz.ioc.Ioc;
import org.nutz.mvc.NutConfig;
import org.nutz.mvc.Setup;

public class MainSetup implements Setup {

    public void init(NutConfig nc) {
        // 取出Dao,顺便校验一下数据库配置是否正确
        Ioc ioc = nc.getIoc();
        ioc.get(Dao.class);
    }

    public void destroy(NutConfig nc) {
    }

}
