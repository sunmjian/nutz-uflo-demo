package net.wendal.uflo;

import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.SetupBy;

@Fail("http:500")
@Ok("json:full")
@IocBy(args= {"*js", "ioc/",
              "*anno", "net.wendal.uflo"})
@SetupBy(MainSetup.class)
public class MainModule {
}
