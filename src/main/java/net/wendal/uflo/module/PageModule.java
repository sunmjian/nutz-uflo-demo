package net.wendal.uflo.module;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

@IocBean
public class PageModule {

    // 首页地址, 自己改成需要的!! 默认是跳转到uflo的设计器页面
    @Ok(">>:/uflo/designer")
    @At("/")
    public void index() {}
}
