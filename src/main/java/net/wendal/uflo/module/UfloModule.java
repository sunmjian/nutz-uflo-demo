package net.wendal.uflo.module;

import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import com.bstek.uflo.service.TaskService;

@At("/ext/uflo")
@IocBean
public class UfloModule {
    
    // 可以注入uflo的服务类
    @Inject
    protected TaskService taskService;

    @Ok(">>:/uflo/designer")
    @At("/")
    public void index() {
    }
}
