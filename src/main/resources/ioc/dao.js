var ioc = {
	conf : {
		type : "org.nutz.ioc.impl.PropertiesProxy",
		args : ["custom/"]
	},
	dao : {
		type : "org.nutz.dao.impl.NutDao",
		args : [{refer:"dataSource"}]
	},
	dataSource : {
		type : "javax.sql.DataSource",
		factory : "$conf#make",
		args : ["com.alibaba.druid.pool.DruidDataSource", "db."],
		events : {
			depose : "close"
		}
	}
};