# nutz-uflo-demo
Nutz集成Uflo的示例项目

UFLO2是一款纯Java流程引擎

这是与[nutz mvc](https://github.com/nutzam/nutz)的集成项目

如果使用[nutzboot](https://gitee.com/nutz/nutzboot), 有nutzboot-starter-uflo,集成更简单更方便,直接添加依赖就行!!

## 使用说明

1. 这是一个maven项目!!!
2. 数据库信息在db.properties, 仅测试过mysql. 因为uflo基于hibernate读写数据库,理论上支持任意数据库.
3. uflo的配置项在uflo.properties
4. 必看UfloEnvironmentProvider类!!

## 运行说明

```
mvn clean compile jetty:run
```

## 如需帮助

nutz相关的问题: 访问 https://nutz.cn 发帖!
nutz qq群: 68428921
uflo: https://github.com/youseries/uflo

## License
Apache-2.0